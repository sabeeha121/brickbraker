package brickbraker;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Gameplay  extends JPanel implements KeyListener, ActionListener {
    

       private boolean play= false;
    private int score = 0;
    
    private int totalBricks = 42;
    
    private Timer timer;
    private int delay =8;
    
    private  int playerX =310;
    
    private  int ballposX = 120;
    private int ballposY = 350;
   private int ballXdir = -1;
   private int ballYdir = -2;
   
   private MapGenerater map;
     
   
   public Gameplay(){
       map = new MapGenerater(6,7);
        
       addKeyListener(this);
       setFocusable(true);
       setFocusTraversalKeysEnabled(false);
       timer= new Timer(delay, this);
       timer.start();
   }
   public void paint(Graphics g){
       //background
       g.setColor(Color.pink);
       g.fillRect(1,1, 692, 592);
       
       map.draw((Graphics2D)g);
       
       //borders
       g.setColor(Color.yellow);
       g.fillRect(0, 0, 3, 592);
        g.fillRect(0, 0, 692, 3);
         g.fillRect(691, 0, 3, 592);
         
         //score
         g.setColor(Color.green);
         g.setFont(new Font("serif", Font.BOLD, 25));
         g.drawString(""+score, 200, 30);
         
         // the paddle
         g.setColor(Color.green);
         g.fillRect(playerX, 550,100,8);
         
         // the ball
         g.setColor(Color.yellow);
         g.fillOval(ballposX, ballposY, 20, 20);
         
         if(totalBricks<=0) {
       play= false;
       ballXdir =0;
       ballYdir = 0;
			
       g.setColor(Color.red);
       g.setFont(new Font("serif", Font.BOLD, 30));
       g.drawString(" You won and your score is: "+ score, 400, 400);
			
       g.setFont(new Font("serif", Font.BOLD, 29));
       g.drawString("Press Enter to Restart", 250, 350);
		}
		
       if(ballposY > 570) {
       play= false;
        ballXdir =0;
	ballYdir = 0;
        g.setColor(Color.red);
        g.setFont(new Font("serif", Font.BOLD, 30));
	g.drawString(" GameOver!", 190, 300);
			
	g.setFont(new Font("serif", Font.BOLD, 29));
	g.drawString("Press Enter to Restart", 250, 350);
			
			
				
	}
			
         
         g.dispose();
   }
  
 
    @Override
    public void keyTyped(KeyEvent e) { }
     @Override
    public void keyReleased(KeyEvent e) { }
    @Override
    

    
    public void keyPressed(KeyEvent e) {
          if(e.getKeyCode()== KeyEvent.VK_RIGHT){
           if(playerX >=600){
               playerX = 600;
           } else{
               moveRight();
           }
       }
       if(e.getKeyCode()== KeyEvent.VK_LEFT){
            if(playerX < 10){
               playerX = 10;
           } else{
               moveLeft();
       }
   }
     if(e.getKeyCode() == KeyEvent.VK_ENTER) {
     if(!play) {
      play = true;
      ballposX =120;
      ballposY = 350;
      ballXdir = -1;
      ballYdir = -2;
      playerX = 310;
      score= 0;
      totalBricks =42;
      map = new MapGenerater(6, 7);
			
      repaint();
		}
	}
	}

    private void moveRight() {
        play = true;
        playerX += 20;
    }

    private void moveLeft() {
         play = true;
        playerX-= 20;
   }
    


    @Override
    public void actionPerformed(ActionEvent e) {
       timer.start();
       
       if(play){
           if(new Rectangle(ballposX, ballposY, 20, 20).intersects(new Rectangle(playerX, 550, 100,8))){
               ballYdir = -ballYdir;
           }
           A: for(int i =0; i< map.map.length; i++) {
	 for( int j = 0; j<map.map[0].length; j++) {
	 if(map.map[i][j] >0) {
        int brickX = j * map.brickwidth + 80;
       int brickY = i * map.brickheight + 50;
       int brickwidth = map.brickwidth;
       int brickheight = map.brickheight;
					 
       Rectangle rect = new Rectangle(brickX, brickY, brickwidth, brickheight);
        Rectangle ballRect = new Rectangle(ballposX, ballposY, 20, 20);
        Rectangle brickRect = rect;
					 
        if(ballRect.intersects(brickRect)) {
        map.setBrickValue(0, i, j);
	 totalBricks--;
         score += 5;
						 
        if(ballposX +19 <= brickRect.x || ballposX + 1>= brickRect.x + brickRect.width) {
	 ballXdir = -ballXdir;
							 
	 }else {
	 ballYdir= -ballYdir;
	 }
    break A;
						 
  }
					 
    }
				 
	 }
	}
               
           //left border
           ballposX += ballXdir;
           ballposY += ballYdir;
           //top border
           if(ballposX < 0){
               ballXdir = -ballXdir;
           }
           //right border
             if(ballposY < 0){
               ballYdir = -ballYdir;
            }
              if(ballposX > 670){
               ballXdir = -ballXdir;
           }
       repaint();
    }
    
}}
